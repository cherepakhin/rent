## Тестовое задание "Аренда автомобилей"

[Само задание](doc/task_description.pdf)

#### Модель

[Арендуемый автомобиль](src/main/java/ru/perm/v/rent/model/Car.java)
[Марка автомобиля](src/main/java/ru/perm/v/rent/model/ModelCar.java)
[Пункт проката](src/main/java/ru/perm/v/rent/model/RentalPoint.java)
[Текущий статус автомобиля](src/main/java/ru/perm/v/rent/model/Status.java)
[Сведения об аренде](src/main/java/ru/perm/v/rent/model/RentHistory.java)

