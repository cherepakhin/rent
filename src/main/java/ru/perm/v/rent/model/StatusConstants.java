package ru.perm.v.rent.model;

/**
 * Константы для статуса машины
 */
public interface StatusConstants {

	Long FREE = 0L; // Можно арендовать
	Long RENT = 1L; // Арендована
}